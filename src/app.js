import CalendarController from './controllers/calendar.js';
import CalendarView from './templates/calendar.js';
// Calendar parameters
const now = new Date();
const day = now.toLocaleString('en-us', { weekday: 'long' });
const calendarParams = { day };
// Instantiate a super simple MVC implementation
const $body = document.querySelector('body');
const model = [];
const controller = new CalendarController(model);
const view = new CalendarView(controller, $body, calendarParams);
/**
 * Render the given events
 * @param {object[]} events Events
 */
window.renderEvents = function(events) {
  controller.model = events;
}
