const DAY_START_HOUR = 9;
const MINUTES_PER_HOUR = 60;
const MINUTES_PER_DAY = 720; // Day is 0900 - 2100
/**
 * Annotate intervals using Quick & Dirty checking
 * 
 * 1. Loop over each minute on         i₀ i₁ i₂ i₃…
 *    the timeline.                  0          ▊
 *                                   1 ▊--▊--------x
 * 2. For the current minute x,      2    ▊  ▊
 *    count interval collisions      3
 *    where iˢ <= x <= iᵉ            …
 *
 * 3. Note collisions and set-relative positions at every collision group
 *    boundary
 *
 * @param {Array<Array>} intervals List of intervals to check [iˢ, iᵉ]
 */
function annotateIntervals(intervals) {
  let intervalGroup = new Set();

  for (let i = 0; i < MINUTES_PER_DAY; i++) {
    const newIntervals = [];
    let totalIntervals = 0;
    // Check if the current minute is within any of the intervals
    intervals.forEach(interval => {
      const [iStart, iEnd] = interval;

      if (iStart <= i && i <= iEnd) {
        totalIntervals++;

        if (!intervalGroup.has(interval)) {
          newIntervals.push(interval);
        } else {
          intervalGroup.add(interval);
        }
      }
    });
    // When no intervals are found or those found are not in the collision set,
    // annotate the intervals in the collision set and clear it
    if (totalIntervals && newIntervals.length === totalIntervals) {
      Array.from(intervalGroup).forEach((interval, index) => {
        const count = interval[2] || 0;
        const position = interval[3] || 0;

        interval[2] = Math.max(count, intervalGroup.size);
        interval[3] = Math.max(position, index);
      });
      // Clear the set
      intervalGroup.clear();
    }

    newIntervals.forEach(interval => intervalGroup.add(interval));
  }
}
/**
 * Transforms and annotates the given list of events to an XML document
 * 
 * @param {object[]} events
 * @return {XMLDocument} XML representation of the given event list
 */
export default function(events = []) {
  const xmlDoc = document.implementation.createDocument('', '');
  const rootElement = xmlDoc.createElement('events');
  let intervals;
  // Sort events by starts_at, map to intervals to check for collisions
  events.sort((a, b) => a.starts_at - b.starts_at);
  intervals = events.map(event => [ event.starts_at, event.starts_at + event.duration ]);
  annotateIntervals(intervals);

  events.forEach((event, index) => {
    const end = new Date();
    const endHours = Math.floor((event.starts_at + event.duration) / MINUTES_PER_HOUR) + DAY_START_HOUR;
    const endMinutes = (event.starts_at + event.duration) % MINUTES_PER_HOUR;
    const eventElement = xmlDoc.createElement('event');
    const eventWidth = 1 / intervals[index][2] * 100;
    const eventPosition = intervals[index][3] * eventWidth;
    const start = new Date();
    const startHours = Math.floor(event.starts_at / MINUTES_PER_HOUR) + DAY_START_HOUR;
    const startMinutes = event.starts_at % MINUTES_PER_HOUR;

    start.setHours(startHours, startMinutes, 0);
    end.setHours(endHours, endMinutes, 0);
    event.startTime = start.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' });
    event.ends_at = event.starts_at + event.duration;
    event.endTime = end.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' });
    event.w = `${ eventWidth }%`;
    event.position = `${ eventPosition }%`;

    Object.entries(event).forEach(([key, value]) => eventElement.setAttribute(key, value));
    rootElement.appendChild(eventElement);
  });

  xmlDoc.appendChild(rootElement);
  return xmlDoc;
};