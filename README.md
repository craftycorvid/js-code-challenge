
# JavaScript Code Challenge Solution

## Running the solution

1. `npm install && npm start` for your convenience, or copy to your own web server
2. Navigate to http://localhost:3000/

## Architecture

The solution yields a day-view calendar by applying an architecture consisting of:

* HTML and CSS documents
* MVC-structured JavaScript modules
* XSL templates

### Components

I chose these components because they demonstrate that a dynamic web application
can be built with browser-native features.

Content is made flexible by modeling a document with XSLT and applying data, in 
much the same way as JSX or Handlebars. Presentation is made flexible by
incorporating CSS custom properties (e.g. `--event-start`) and thoughtfully-
arranged style rules. The logic orchestrating the experience leverages modern
JavaScript, DOM, and standard browser APIs to load resources, transform data,
and render output.

Events given to _window.renderEvents()_ are:
1. passed to the _calendar_ controller, analyzed and annotated for collisions,
   transformed to XML, then
2. rendered to the document by the _calendar_ view, and
3. positioned on a minute-aligned grid by the document's CSS rules

### Collisions

The known timeline for a day is scanned for collisions by checking whether
each minute is within multiple event intervals. If a collision is encountered,
the interval is added to a set. When the minute is within no event intervals or
intervals of the next set, the encountered intervals are annotated with the
number of collisions in the current set and their position relative to the
first. The set is cleared and the process begins again.

Interval collision algorithms seem to be well-studied. A more effective approach
could involve an [interval tree][1] data structure and be performed server-side
for scale and caching concerns.

## Targets

* Latest Chrome
* Latest Edge
* Latest Firefox
* Latest Safari

## Building

There is no build step or any dependencies, but the browser will expect
resources to be served from the (local) network.


[1]: https://en.wikipedia.org/wiki/Interval_tree "Interval Tree | Wikipedia"